'use strict';

angular.module('developerChallengeApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ngAnimate',
    'ui.bootstrap'
  ])
.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
        templateUrl: 'partials/main',
        controller: 'MainCtrl'
      })
        .when('/title/:id', {
        templateUrl: 'partials/title',
        controller: 'TitleCtrl'
      })
        .otherwise({
        redirectTo: '/'
      });
    $locationProvider.html5Mode(true);
  })
    .directive('search', function() {
    return function(scope, element) {

        element.focus();

        var timeout;
        element.bind('keydown', function() {
            if (timeout) {
              clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
                scope.$apply(function() {
                    scope.getTitlesByName(element.val());
                  });
              }, 500);
          });
      };
  });