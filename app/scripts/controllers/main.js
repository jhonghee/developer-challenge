'use strict';

angular.module('developerChallengeApp')
    .controller('MainCtrl', function($scope, $http) {

    $scope.secondaryFilter = function(title) {
        var colorCnt = 1;
        if ($scope.keywords.length > 0 && $scope.keywords[0]) {
          var re = new RegExp($scope.keywords[0], 'gi');
          title.TitleNameHighlighted = title.TitleName.replace(re, function(match) {
            return '<span class="highlighted-' + colorCnt + '">' + match + '</span>';
          });
        }

        return _.reduce($scope.keywords, function(found, keyword) {
            return found && S(title.TitleName.toLowerCase()).contains(keyword.toLowerCase());
          }, true);
      };

    $scope.getTitlesByName = function(searchString) {
        $scope.keywords = searchString.split(/[ ,]+/);
        if ($scope.keywords.length > 0 && $scope.keywords[0]) {
          $http.get('/api/titles/' + $scope.keywords[0]).success(function(titles) {
            $scope.titles = titles;
            if (titles.length > 0) {
              $scope.noMatches = 'none';
            } else {
              $scope.noMatches = 'block';
            }
          });
        } else {
          $scope.titles = [];
          $scope.noMatches = 'block';
        }
      };
    $scope.titlePopover = function(title) {
        var titleStr = title.TitleType;
        titleStr += ' / ' + title.ReleaseYear;
        titleStr += ', ' + title.AnimationMode;
        titleStr += ', ' + title.Genres[0];
        for(var i=1; i<title.Genres.length; i++) {
          titleStr += ' ' + title.Genres[i];
        }
        return titleStr;
      };
  })
    .controller('TitleCtrl', function($scope, $http, $routeParams) {

      $scope.awardWon = function(award) {
        return award.AwardWon;
      };

      $http.get('/api/title/' + $routeParams.id).success(function(title) {
        $scope.title = title;
      });
    });