'use strict';

var mongoose = require('mongoose'),
    Title = mongoose.model('Title'),
    async = require('async');

exports.getTitlesByName = function(req, res) {
	return Title.find({ TitleName: new RegExp(req.params.name, 'i') }, 'TitleName TitleType AnimationMode ReleaseYear Genres', function (err, titles) {
		if (!err) {
			return res.json(titles);
		} else {
			return res.send(err);
		}
	});
};

exports.getTitleById = function(req, res) {
	return Title.findById(req.params.id, function(err, title){
		console.log(req.params.id);
		console.log(title);
		if( !err) {
			return res.json(title);
		} else {
			return res.send(err);
		}
	});
};