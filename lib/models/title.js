'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// Schema
var TitleSchema = new Schema({
  _id: String,
  TitleName: String
});

mongoose.model('Title', TitleSchema);
